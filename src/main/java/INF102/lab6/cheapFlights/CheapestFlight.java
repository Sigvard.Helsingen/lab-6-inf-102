package INF102.lab6.cheapFlights;

import java.util.*;

import INF102.lab6.graph.DirectedEdge;
import INF102.lab6.graph.WeightedDirectedGraph;

public class CheapestFlight implements ICheapestFlight {


    @Override
    public WeightedDirectedGraph<City, Integer> constructGraph(List<Flight> flights) {
        WeightedDirectedGraph<City, Integer> directedGraph = new WeightedDirectedGraph<City, Integer>();
        for(Flight flight : flights){
            directedGraph.addVertex(flight.start);
            directedGraph.addVertex(flight.destination);
            directedGraph.addEdge(flight.start, flight.destination, flight.cost);
        }
        return directedGraph;
    }


    @Override
    public int findCheapestFlights(List<Flight> flights, City start, City destination, int nMaxStops) {
        WeightedDirectedGraph<City, Integer> directedGraph = constructGraph(flights);
        Map<City, Integer> prices = dijkstra(directedGraph, start, nMaxStops);
        return prices.get(destination);
    }

    private Map<City,Integer> dijkstra(WeightedDirectedGraph<City, Integer> directedGraph, City start, int nMaxStops){
        Map<nStops, Trip> bestPrice = new HashMap<>();
        PriorityQueue<Trip> ts = new PriorityQueue<>();
        nStops startReach = new nStops(start, 0 );
        Trip startTrip = new Trip(startReach, 0);
        addNeighbours(startTrip, directedGraph, ts);
        bestPrice.put(startReach, startTrip);

        while (!ts.isEmpty()){
            Trip current = ts.remove();
            if (bestPrice.containsKey(current.destInStops)){
                continue;
            }
            bestPrice.put(current.destInStops, current);
            if(current.destInStops.stops <= nMaxStops){
                addNeighbours(current, directedGraph, ts);
            }

        }
        Map<City, Integer> distances = new HashMap<>();
        for(Trip trip : bestPrice.values()){
            City city = trip.destInStops.dest;
            int price = trip.totPrice;
            if(trip.destInStops.stops > nMaxStops+1){
                continue;
            }
            if (!distances.containsKey(city) || distances.get(city) > price){
                distances.put(city, price);
            }
        }
        return distances;
    }

    private void addNeighbours(Trip currTrip, WeightedDirectedGraph<City, Integer> directedGraph, PriorityQueue<Trip> q) {
        for(City city : directedGraph.outNeighbours(currTrip.destInStops.dest)){
            nStops currentget = new nStops(city, currTrip.destInStops.stops+1);
            Trip trip = new Trip(currentget, currTrip.totPrice + directedGraph.getWeight(currTrip.destInStops.dest, city));
            q.add(trip);

        }
    }

    class nStops{
        City dest;
        Integer stops;

        public nStops(City c, Integer stops){
            this.dest = c;
            this.stops = stops;
        }
        @Override
        public boolean equals(Object o){
            if (o == this){
                return true;
            }
            if(!(o instanceof nStops)){
                return false;
            }
            nStops other = (nStops) o;
            return this.dest.equals(other.dest) && this.stops.equals(other.stops);

        }

        @Override
        public int hashCode(){
            return Objects.hash(dest, stops);
        }
    }
    class Trip implements Comparable<Trip> {
        nStops destInStops;
        Integer totPrice;

        public Trip(nStops reach, Integer price){
            this.destInStops = reach;
            this.totPrice = price;
        }

        public int compareTo(Trip o){
            return Integer.compare(totPrice, o.totPrice);
        }
    }

}
